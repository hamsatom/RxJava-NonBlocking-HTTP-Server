package ass.hamsatom.semestral.response;

import ass.hamsatom.semestral.Creator;
import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.constat.HttpMethod;
import ass.hamsatom.semestral.request.ParsedRequest;
import ass.hamsatom.semestral.util.Cache;
import java.util.Optional;
import javax.annotation.Nonnull;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 13.04.2017.
 */
public class CacheResponseTest {

  @Test
  public void testGetClient() {
    ParsedRequest parsedRequest = Creator.createValidRequest();
    Response response = new CacheResponse(parsedRequest);
    Assert.assertNotNull(response.getClient());
  }

  @Test
  public void testGetCodeInvalid() {
    ParsedRequest parsedRequest = Creator.createInvalidRequest();
    Response response = new CacheResponse(parsedRequest);
    Assert.assertEquals(response.getCode(), HttpCode.INTERNAL_SERVER_ERROR);
  }

  @Test
  public void testGetDataFromInvalid() {
    ParsedRequest parsedRequest = Creator.createInvalidRequest();
    Response response = new CacheResponse(parsedRequest);
    Assert.assertEquals(response.getData(), Optional.empty());
  }

  @Test
  public void testGetMethod() {
    HttpMethod method = HttpMethod.GET;
    ParsedRequest request = Creator.createRequest(Creator.TEST_FILE, "", "", method);
    CacheResponse response = new CacheResponse(request);
    Assert.assertEquals(response.getMethod(), method);
  }

  private boolean cacheHasRequest(@Nonnull ParsedRequest parsedRequest) {
    return Cache.containsValidData(parsedRequest.getKey().toString());
  }

  @Test
  public void testIsCacheableFromInvalid() {
    ParsedRequest parsedRequest = Creator.createInvalidRequest();
    Response response = new CacheResponse(parsedRequest);
    Assert.assertFalse(response.isCacheable());
  }

  @Test
  public void testIsValid() {
    ParsedRequest parsedRequest = Creator.createValidRequest();
    Response response = new CacheResponse(parsedRequest);
    Assert.assertEquals(response.isValid(), cacheHasRequest(parsedRequest));
  }

  @Test
  public void testIsValidFromInvalidRequest() {
    ParsedRequest parsedRequest = Creator.createInvalidRequest();
    Response response = new CacheResponse(parsedRequest);
    Assert.assertFalse(response.isValid());
  }
}