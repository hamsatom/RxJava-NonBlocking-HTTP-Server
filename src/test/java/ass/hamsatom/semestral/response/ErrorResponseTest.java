package ass.hamsatom.semestral.response;

import ass.hamsatom.semestral.Creator;
import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.constat.HttpMethod;
import ass.hamsatom.semestral.request.ParsedRequest;
import java.util.Optional;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 12.04.2017.
 */
public class ErrorResponseTest {

  @Test
  public void testGetCode() {
    HttpCode code = HttpCode.BAD_REQUEST;
    Response response = new ErrorResponse(code, Creator.createValidRequest());
    Assert.assertEquals(response.getCode(), code);
  }

  @Test
  public void testGetData() {
    Response response = new ErrorResponse(HttpCode.BAD_REQUEST,
        Creator.createValidRequest());
    Assert.assertEquals(response.getData(), Optional.empty());
  }

  @Test
  public void testGetClient() {
    ParsedRequest parsedRequest = Creator.createValidRequest();
    Response response = new ErrorResponse(HttpCode.BAD_REQUEST, parsedRequest);
    Assert.assertNotNull(response.getClient());
  }

  @Test
  public void testGetMethod() {
    HttpMethod method = HttpMethod.GET;
    ParsedRequest request = Creator.createRequest(Creator.TEST_FILE, "", "", method);
    Response response = new ErrorResponse(HttpCode.BAD_REQUEST, request);
    Assert.assertEquals(response.getMethod(), method);
  }

  @Test
  public void testIsCacheable() {
    Response response = new ErrorResponse(HttpCode.BAD_REQUEST,
        Creator.createValidRequest());
    Assert.assertFalse(response.isCacheable());
  }

  @Test
  public void testIsValid() {
    Response response = new ErrorResponse(HttpCode.BAD_REQUEST,
        Creator.createValidRequest());
    Assert.assertFalse(response.isValid());
  }
}