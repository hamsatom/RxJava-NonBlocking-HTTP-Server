package ass.hamsatom.semestral.util;

import java.util.Optional;
import java.util.UUID;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 12.04.2017.
 */
public class CacheTest {

  @Test
  public void testContainsValidData() {
    Assert.assertFalse(Cache.containsValidData("notPresent.txt"));
  }

  @Test
  public void testGetData() {
    Optional<byte[]> data = Cache.getData("nonpresent.txt");
    Assert.assertFalse(data.isPresent());
  }

  @Test
  public void testGetPresentData() {
    String key = "presentData.txt";
    byte[] value = new byte[0];
    Cache.cacheInMemory(key, value);
    Optional<byte[]> data = Cache.getData(key);
    Assert.assertEquals(data.get(), value);
  }

  @Test
  public void testCacheInMemory() {
    String key = "presentData.txt";
    Cache.cacheInMemory(key, new byte[0]);
    Assert.assertTrue(Cache.containsValidData(key));
  }

  @Test
  public void testCacheManyInMemory() {
    for (int i = 0; i < 10000; i++) {
      String key = UUID.randomUUID().toString();
      byte[] value = key.getBytes();
      Cache.cacheInMemory(key, value);
    }
  }
}