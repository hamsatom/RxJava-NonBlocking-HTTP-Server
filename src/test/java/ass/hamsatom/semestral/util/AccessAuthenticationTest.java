package ass.hamsatom.semestral.util;

import org.testng.Assert;
import org.testng.annotations.Test;

import ass.hamsatom.semestral.Creator;
import ass.hamsatom.semestral.constat.HttpMethod;
import ass.hamsatom.semestral.request.ParsedRequest;

/**
 * @author Tomáš Hamsa on 12.04.2017.
 */
public class AccessAuthenticationTest {

  @Test
  public void testHasAccess() throws Exception {
    ParsedRequest parsedRequest = Creator
        .createRequest(Creator.PROTECTED_FILE, Creator.PASSWORD, Creator.USERNAME,
            HttpMethod.GET);
    Assert.assertTrue(AccessAuthentication.hasAccess(Creator.ROOT, parsedRequest));
  }

  @Test
  public void testNoAccess() throws Exception {
    ParsedRequest parsedRequest = Creator
        .createRequest(Creator.PROTECTED_FILE, "", Creator.USERNAME, HttpMethod.GET);
    Assert.assertFalse(AccessAuthentication.hasAccess(Creator.ROOT, parsedRequest));
  }

  @Test
  public void testAccessNoUsername() throws Exception {
    ParsedRequest parsedRequest = Creator
        .createRequest(Creator.PROTECTED_FILE, Creator.PASSWORD, "", HttpMethod.GET);
    Assert.assertFalse(AccessAuthentication.hasAccess(Creator.ROOT, parsedRequest));
  }

  @Test
  public void testAccessBlank() throws Exception {
    ParsedRequest parsedRequest = Creator
        .createRequest(Creator.PROTECTED_FILE, "", "", HttpMethod.GET);
    Assert.assertFalse(AccessAuthentication.hasAccess(Creator.ROOT, parsedRequest));
  }

  @Test
  public void testAccessUnprotected() throws Exception {
    ParsedRequest parsedRequest = Creator
        .createRequest(Creator.TEST_FILE, "", "", HttpMethod.GET);
    Assert.assertFalse(AccessAuthentication.hasAccess(Creator.ROOT, parsedRequest));
  }

  @Test
  public void testAccessUnprotectedWithPasswordAndUsername() throws Exception {
    ParsedRequest parsedRequest = Creator
        .createRequest(Creator.TEST_FILE, Creator.PASSWORD + "difference",
            Creator.USERNAME + "difference", HttpMethod.GET);
    Assert.assertFalse(AccessAuthentication.hasAccess(Creator.ROOT, parsedRequest));
  }
}