package ass.hamsatom.semestral.parser;

import ass.hamsatom.semestral.Creator;
import ass.hamsatom.semestral.constat.Constants;
import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.constat.HttpMethod;
import ass.hamsatom.semestral.request.HttpRequest;
import ass.hamsatom.semestral.request.HttpRequestImpl;
import ass.hamsatom.semestral.response.Response;
import io.reactivex.Observable;
import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Base64;
import java.util.Optional;
import javax.annotation.Nonnull;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 12.05.2017.
 */
public class HttpParserImplTest {

  private Observable<HttpRequest> createHttpRequest(@Nonnull String method,
      @Nonnull String file) throws IOException {
    return Observable.just(new HttpRequestImpl(SocketChannel.open(),
        method + " " + file + " HTTP/1.1" + Constants.HTTP_LINE_SEPARATOR
            + Constants.HTTP_LINE_SEPARATOR));
  }

  @Test
  public void testHeadValid() throws Exception {
    HttpParser parser = Creator.getParser();
    String permission = Creator.USERNAME + ":" + Creator.PASSWORD;
    String httpPermission = Base64.getEncoder().encodeToString(permission.getBytes());
    HttpRequest request = new HttpRequestImpl(SocketChannel.open(),
        "HEAD " + Creator.PROTECTED_FILE + " HTTP/1.1" + Constants.HTTP_LINE_SEPARATOR
            + "Authorization: Basic " + httpPermission + Constants.HTTP_LINE_SEPARATOR
            + Constants.HTTP_LINE_SEPARATOR);
    Observable.just(request).compose(parser)
        .map(response -> response.isValid() && response.getCode() == HttpCode.OK
            && response.getMethod() == HttpMethod.HEAD && response.getData().isPresent())
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

  @Test
  public void testPostValid() throws Exception {
    HttpParser parser = Creator.getParser();
    createHttpRequest("POST", Creator.TEST_FILE).compose(parser)
        .map(response -> !response.isValid() && response.getCode() == HttpCode.NOT_IMPLEMENTED
            && response.getMethod() == HttpMethod.POST)
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

  @Test
  public void testHeadProtectedWithoutPermission() throws Exception {
    HttpParser parser = Creator.getParser();
    HttpRequest request = new HttpRequestImpl(SocketChannel.open(),
        "HEAD " + Creator.PROTECTED_FILE + " HTTP/1.1" + Constants.HTTP_LINE_SEPARATOR
            + Constants.HTTP_LINE_SEPARATOR);
    Observable.just(request).compose(parser)
        .map(response -> !response.isValid() && response.getCode() == HttpCode.UNAUTHORIZED
            && response.getMethod() == HttpMethod.HEAD && !response.getData().isPresent())
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

  @Test
  public void testGetWithPermission() throws Exception {
    HttpParser parser = Creator.getParser();
    String permission = Creator.USERNAME + ":" + Creator.PASSWORD;
    String httpPermission = Base64.getEncoder().encodeToString(permission.getBytes());
    HttpRequest request = new HttpRequestImpl(SocketChannel.open(),
        "GET " + Creator.PROTECTED_FILE + " HTTP/1.0" + Constants.HTTP_LINE_SEPARATOR
            + "Authorization: Basic " + httpPermission + Constants.HTTP_LINE_SEPARATOR
            + Constants.HTTP_LINE_SEPARATOR);
    Observable.just(request).compose(parser)
        .map(response -> response.isValid() && response.getCode() == HttpCode.OK
            && response.getMethod() == HttpMethod.GET
            && response.getData().isPresent())
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

  @Test
  public void testGetRoot() throws Exception {
    HttpParser parser = Creator.getParser();
    byte[] indexData = Files.readAllBytes(Paths.get(Creator.INDEX_HTML));
    String permission = Creator.USERNAME + ":" + Creator.PASSWORD;
    String httpPermission = Base64.getEncoder().encodeToString(permission.getBytes());
    HttpRequest request = new HttpRequestImpl(SocketChannel.open(),
        "GET " + Creator.PROTECTED_FILE + " HTTP/1.0" + Constants.HTTP_LINE_SEPARATOR
            + "Authorization: Basic " + httpPermission + Constants.HTTP_LINE_SEPARATOR
            + Constants.HTTP_LINE_SEPARATOR);
    Observable.just(request).compose(parser)
        .map(Response::getData)
        .map(Optional::get)
        .map(data -> Arrays.equals(data, indexData))
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

  @Test
  public void testHeadWindowsFile() throws Exception {
    HttpParser parser = Creator.getParser();
    createHttpRequest("HEAD ", "C:/windows/system32/MessagingService.dll").compose(parser)
        .map(response -> !response.isValid() && response.getCode() == HttpCode.NOT_FOUND
            && response.getMethod() == HttpMethod.HEAD && !response.getData().isPresent())
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

  @Test
  public void testGetUserData() throws Exception {
    HttpParser parser = Creator.getParser();
    String permission = Creator.USERNAME + ":" + Creator.PASSWORD;
    String httpPermission = Base64.getEncoder().encodeToString(permission.getBytes());
    HttpRequest request = new HttpRequestImpl(SocketChannel.open(),
        "GET " + Creator.HTACCESS + " HTTP/1.0" + Constants.HTTP_LINE_SEPARATOR
            + "Authorization: Basic " + httpPermission + Constants.HTTP_LINE_SEPARATOR
            + Constants.HTTP_LINE_SEPARATOR);
    Observable.just(request).compose(parser)
        .map(response -> !response.isValid() && response.getCode() == HttpCode.NOT_FOUND
            && response.getMethod() == HttpMethod.GET
            && !response.getData().isPresent())
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

  @Test
  public void testNonExistent() throws Exception {
    HttpParser parser = Creator.getParser();
    createHttpRequest("NONEXISTING_METHOD ", Creator.TEST_FILE).compose(parser)
        .map(response -> !response.isValid() && response.getCode() == HttpCode.BAD_REQUEST
            && response.getMethod() == HttpMethod.NOT_SUPPORTED && !response.getData()
            .isPresent())
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

  @Test
  public void testGetPicture() throws Exception {
    byte[] indexData = Files.readAllBytes(Paths.get(Creator.PICTURE));
    HttpParser parser = Creator.getParser();
    String permission = Creator.USERNAME + ":" + Creator.PASSWORD;
    String httpPermission = Base64.getEncoder().encodeToString(permission.getBytes());
    HttpRequest request = new HttpRequestImpl(SocketChannel.open(),
        "GET " + Creator.PICTURE + " HTTP/1.1" + Constants.HTTP_LINE_SEPARATOR
            + "Authorization: Basic " + httpPermission + Constants.HTTP_LINE_SEPARATOR
            + Constants.HTTP_LINE_SEPARATOR);
    Observable.just(request).compose(parser)
        .map(Response::getData)
        .map(Optional::get)
        .map(data -> Arrays.equals(data, indexData))
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

}