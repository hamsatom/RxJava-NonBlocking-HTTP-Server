package ass.hamsatom.semestral.constat;

/**
 * Symbolized HTTP method provided in HTTP header
 *
 * @author Tomáš Hamsa on 11.05.2017.
 */
public enum HttpMethod {
  /**
   * Get method
   */
  GET,
  /**
   * Post method
   */
  POST,
  /**
   * Head method
   */
  HEAD,
  /**
   * Any other method
   */
  NOT_SUPPORTED
}
