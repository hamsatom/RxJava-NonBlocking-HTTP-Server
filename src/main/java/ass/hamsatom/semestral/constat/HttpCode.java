package ass.hamsatom.semestral.constat;


/**
 * Constants holder with http codes of responses. All of the codes are from RFC 2616 standard
 *
 * @author Tomáš Hamsa on 14.05.2017.
 */
public enum HttpCode {

  /**
   * HTTP response OK with code 200
   */
  OK(200),

  /**
   * HTTP response Bad request with code 400
   */
  BAD_REQUEST(400),

  /**
   * HTTP response Unauthorized with code 401
   */
  UNAUTHORIZED(401),

  /**
   * HTTP response Not found with code 404
   */
  NOT_FOUND(404),

  /**
   * HTTP response Internal server error with code 500
   */
  INTERNAL_SERVER_ERROR(500),

  /**
   * HTTP response Not implemented for unknown HTTP method with code 501
   */
  NOT_IMPLEMENTED(501);

  private final int code;

  /**
   * Creates new enum HTTP code with matching code
   *
   * @param code Code of HTTP response
   */
  HttpCode(int code) {
    this.code = code;
  }

  /**
   * Provides int value of enum
   *
   * @return Value of code as decimal number
   */
  public int getValue() {
    return code;
  }

  /**
   * Provides name for code with specific value
   *
   * @return Name of response for code e.g. OK for 200
   */
  @Override
  public String toString() {
    char[] name = name().toLowerCase().toCharArray();
    name[0] = Character.toUpperCase(name[0]);
    for (int i = 1; i < name.length; i++) {
      if (name[i - 1] == '_') {
        name[i - 1] = ' ';
        name[i] = Character.toUpperCase(name[i]);
      }
    }
    return new String(name);
  }
}
