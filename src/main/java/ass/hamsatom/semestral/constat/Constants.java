package ass.hamsatom.semestral.constat;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Constants holder with constants the are used in multiple classes
 *
 * @author Tomáš Hamsa on 11.05.2017.
 */
public final class Constants {

  /**
   * Name of file that contains username and password that have access to specified folder
   */
  public static final String AUTHORIZATION_FILE = ".htaccess";

  /**
   * Telnet should send ASCII
   */
  public static final Charset TELNET_CHARSET = StandardCharsets.US_ASCII;

  /**
   * Http requests have line separator \r\n according to standard
   */
  public static final String HTTP_LINE_SEPARATOR = "\r\n";

  /**
   * Root folder annotation text
   */
  public static final String ROOT_FOLDER_ANNOTATION = "root_folder";

  /**
   * As this is utility class with static methods and field this class should not be ever
   * initialized
   *
   * @throws IllegalAccessException if someone tries to initialize this class
   */
  private Constants() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing constants holder");
  }

}
