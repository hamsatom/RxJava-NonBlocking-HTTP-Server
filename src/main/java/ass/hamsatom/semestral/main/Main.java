package ass.hamsatom.semestral.main;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.file.Paths;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;

import ass.hamsatom.semestral.constat.Constants;
import ass.hamsatom.semestral.filter.RequestFilter;
import ass.hamsatom.semestral.filter.RequestFilterImpl;
import ass.hamsatom.semestral.handler.RequestHandler;
import ass.hamsatom.semestral.handler.RequestHandlerImpl;
import ass.hamsatom.semestral.parser.HttpParserImpl;
import ass.hamsatom.semestral.server.Server;
import ass.hamsatom.semestral.server.ServerImpl;
import ass.hamsatom.semestral.writer.AsyncWritingQueue;
import ass.hamsatom.semestral.writer.ResponseBuilderImpl;
import ass.hamsatom.semestral.writer.WritingQueue;

/**
 * Start the server
 */
public class Main {

  private static final Logger LOG = LoggerFactory.getLogger(Main.class);

  /**
   * Default host for server
   */
  private static final String HOST = "localhost";

  /**
   * Default port for server
   */
  private static final int DEFAULT_PORT = 8080;

  /**
   * . stands for current directory so the directory where {@link Main} was executed
   */
  private static final String DEFAULT_PATH = ".";

  /**
   * The smallest port that is out of range
   */
  private static final int HIGHEST_VALID_PORT = 65535;

  /**
   * Port cannot be negative
   */
  private static final byte LOWEST_VALID_PORT = 0;

  /**
   * Starts the server with default configuration
   */
  public static void main(@Nonnull String[] args) throws IOException {
    SocketAddress address = new InetSocketAddress(HOST, extractPort(args));
    String rootFolderName = extractRootFolderName(args);
    Injector inject = Guice.createInjector(new AbstractModule() {
      @Override
      protected void configure() {
        bind(String.class).annotatedWith(Names.named(Constants.ROOT_FOLDER_ANNOTATION))
            .toInstance(rootFolderName);
        bind(SocketAddress.class).toInstance(address);
        bind(RequestHandler.class).to(RequestHandlerImpl.class);
        bind(RequestFilter.class).to(RequestFilterImpl.class);
        bind(WritingQueue.class).to(AsyncWritingQueue.class);
      }
    });
    Server server = inject.getInstance(ServerImpl.class);
    server.run(inject.getInstance(HttpParserImpl.class),
        inject.getInstance(ResponseBuilderImpl.class));
  }

  /**
   * Parses port provided in argument for application. Port should be the first argument.
   *
   * @param args Arguments with which the application was started
   * @return provided port or default port if no valid port was found
   */
  private static int extractPort(@Nonnull String[] args) {
    if (args.length < 1) {
      LOG.warn("No port provided, first application argument should contain port. Using "
          + "default port {}", DEFAULT_PORT);
      return DEFAULT_PORT;
    }

    int providedPort;
    try {
      providedPort = Integer.parseInt(args[0]);
    } catch (@Nonnull NumberFormatException | NullPointerException e) {
      LOG.warn(
          "Provided port is not valid, expected int found " + args[0] + " Using default port "
              + DEFAULT_PORT, e);
      return DEFAULT_PORT;
    }

    if (providedPort <= HIGHEST_VALID_PORT && providedPort >= LOWEST_VALID_PORT) {
      LOG.info("Using port {}", providedPort);
      return providedPort;
    } else {
      LOG.warn("Provided port {} exceeds valid range. Using default port {}", providedPort, DEFAULT_PATH);
      return DEFAULT_PORT;
    }
  }

  /**
   * Parses folder name from application arguments. Folder name is the second application argument.
   *
   * @param args Arguments with which the application was started
   * @return Provided name of the root folder or default root folder if no valid folder was provided
   */
  private static String extractRootFolderName(@Nonnull String[] args) {
    String defaultPath = Paths.get(DEFAULT_PATH).toAbsolutePath().normalize().toString();
    if (args.length < 2) {
      LOG.warn("No root provided, second application argument should contain port. Using "
          + "default root '{}'", defaultPath);
      return DEFAULT_PATH;
    }

    String providedPath = args[1];
    File path = Paths.get(providedPath).toAbsolutePath().normalize().toFile();

    if (path.isDirectory() && path.canRead()) {
      LOG.info("Using root '{}'", path);
      return providedPath;
    } else {
      LOG.warn("Provided root '{}' is not a readable folder. Using default root '{}'", path, defaultPath);
      return DEFAULT_PATH;
    }
  }
}
