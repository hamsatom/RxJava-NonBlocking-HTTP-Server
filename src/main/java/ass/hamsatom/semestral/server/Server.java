package ass.hamsatom.semestral.server;

import java.io.IOException;

import javax.annotation.Nonnull;

import ass.hamsatom.semestral.parser.HttpParser;
import ass.hamsatom.semestral.writer.ResponseBuilder;

/**
 * Http server that process requests and maps the to appropriate http response
 */
public interface Server {

  /**
   * Runs server with given handler
   *
   * @param parser - ObservableTransformer that processes requests
   * @param writer - Consumer that consumes request and writes it to client
   */
  void run(@Nonnull HttpParser parser, @Nonnull ResponseBuilder writer)
      throws IOException;

  /**
   * Stops the server and all components used in it like Response writer
   */
  void stop();
}
