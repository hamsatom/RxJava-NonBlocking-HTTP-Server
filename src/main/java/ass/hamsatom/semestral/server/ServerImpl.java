package ass.hamsatom.semestral.server;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import ass.hamsatom.semestral.constat.Constants;
import ass.hamsatom.semestral.parser.HttpParser;
import ass.hamsatom.semestral.request.HttpRequest;
import ass.hamsatom.semestral.request.HttpRequestImpl;
import ass.hamsatom.semestral.util.ByteBufferUtil;
import ass.hamsatom.semestral.writer.ResponseBuilder;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import io.vavr.control.Try;
import lombok.Cleanup;

/**
 * Http server that process requests and maps the to appropriate http response
 */
public class ServerImpl implements Server {

  private static final Logger LOG = LoggerFactory.getLogger(ServerImpl.class);

  /**
   * Size of byte buffer to which tha request text is buffered. Should be big enough to contain 1MB
   * message
   */
  private static final int LIMIT = 1024 * 1024;
  /**
   * Symbol of end of one http request
   */
  private static final String HTTP_REQUEST_TERMINATOR =
      Constants.HTTP_LINE_SEPARATOR + Constants.HTTP_LINE_SEPARATOR;

  /**
   * Received requests
   */
  @Nonnull
  private final Subject<HttpRequest> requests;

  /**
   * Address that server will bind to
   */
  @Nonnull
  private final SocketAddress address;

  /**
   * Flag that represents if server should stop. False if server should keep running, true if server
   * should stop. Default value of {@code boolean} in Java is {@code false}.
   */
  private volatile boolean exit;

  /**
   * Creates Http server that process requests and maps the to appropriate http response
   *
   * @param address Address on which the server will open it's socket channel
   */
  @Inject
  public ServerImpl(@Nonnull SocketAddress address) {
    this.address = address;
    requests = PublishSubject.create();
  }

  /**
   * Stops the server
   */
  @Override
  public void stop() {
    exit = true;
  }

  @Override
  public void run(@Nonnull HttpParser parser, @Nonnull ResponseBuilder writer)
      throws IOException {
    // open, bind and configure ServerSocketChannel
    @Cleanup ServerSocketChannel serverChannel = ServerSocketChannel.open();
    serverChannel.bind(address);
    serverChannel.configureBlocking(false);

    LOG.debug("Server started on address: {}", address);

    // open selector and register serverSocketChannel to it
    Selector selector = Selector.open();
    serverChannel.register(selector, serverChannel.validOps());

    ByteBuffer buffer = ByteBuffer.allocate(LIMIT);
    Map<Channel, ByteArrayOutputStream> messages = new HashMap<>();

    // processes the received requests
    requests.compose(parser).subscribe(writer);

    // start non-blocking loop
    while (!exit) {
      selector.select();
      Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
      Observable.<SelectionKey>create(emitter -> {
        while (iterator.hasNext()) {
          SelectionKey next = iterator.next();
          emitter.onNext(next);
          iterator.remove();    // this is important, as selector only adds but does
          // not remove
        }
        emitter.onComplete();
      }).filter(SelectionKey::isValid).cache().groupBy(key -> {
        if (key.isReadable()) {
          return KeyStatus.READABLE;
        } else if (key.isAcceptable()) {
          return KeyStatus.ACCEPTABLE;
        } else {
          return KeyStatus.INVALID;
        }
      }).subscribe(labeledRequest -> {
        switch (labeledRequest.getKey()) {
          case ACCEPTABLE:
            // Opens the client channel
            labeledRequest.filter(SelectionKey::isAcceptable)
                .map(key -> (ServerSocketChannel) key.channel())
                .map(ServerSocketChannel::accept)
                .subscribe(channel -> {
                  channel.configureBlocking(false);
                  channel.register(selector, SelectionKey.OP_READ);
                });
            break;
          case READABLE:
            labeledRequest.map(key -> (SocketChannel) key.channel())
                .subscribe(channel -> Try.of(() -> {
                  // Reads text from client channel
                  String messagePart = ByteBufferUtil.readFromChannel(buffer, channel);
                  @Cleanup ByteArrayOutputStream wholeMessage = messages.getOrDefault(channel,
                      new ByteArrayOutputStream(messagePart.length()));
                  wholeMessage.write(messagePart.getBytes());
                  messages.putIfAbsent(channel, wholeMessage);
                  // If all text from request was read add the request to received requests
                  if (messagePart.endsWith(HTTP_REQUEST_TERMINATOR)) {
                    String finalMessage = wholeMessage
                        .toString(Constants.TELNET_CHARSET.name());
                    messages.remove(channel);
                    requests.onNext(new HttpRequestImpl(channel, finalMessage.trim()));
                  }
                  return null;
                }).onFailure(e -> messages.remove(channel)));
            break;
          default:
            break;
        }
      }, e -> LOG.info("Error while maintaining connection", e));
    }
    LOG.debug("Server on address {} stopped", address);
    writer.endWriting();
  }

  /**
   * Represents status of channel that the key represents
   */
  private enum KeyStatus {
    /**
     * It's possible to read from channel
     */
    READABLE,
    /**
     * Channel from client that can be accepted
     */
    ACCEPTABLE,

    /**
     * Represents key that is not valid
     */
    INVALID
  }
}
