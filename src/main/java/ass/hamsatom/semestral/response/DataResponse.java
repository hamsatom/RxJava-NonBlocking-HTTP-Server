package ass.hamsatom.semestral.response;

import java.nio.channels.SocketChannel;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.constat.HttpMethod;
import lombok.Getter;

/**
 * Represents response that contains data. This si different from {@link ErrorResponse} that doesn't
 * contain any data
 *
 * @author Tomáš Hamsa on 14.05.2017.
 */
abstract class DataResponse implements Response {

  /**
   * Data from file specified in {@code originResponse}
   */
  @Nullable
  private byte[] data;

  /**
   * Http method that request this data
   */
  @Getter
  private final HttpMethod method;

  /**
   * Channel from which the request arrived
   */
  @Getter
  private final SocketChannel client;

  DataResponse(@Nonnull HttpMethod method, @Nonnull SocketChannel client) {
    this.method = method;
    this.client = client;
  }

  @Override
  public HttpCode getCode() {
    return isValid() ? HttpCode.OK : HttpCode.INTERNAL_SERVER_ERROR;
  }

  @Nonnull
  @Override
  public Optional<byte[]> getData() {
    return Optional.ofNullable(data);
  }

  void setData(@Nullable byte[] data) {
    this.data = data;
  }

  @Override
  public boolean isValid() {
    return data != null;
  }
}
