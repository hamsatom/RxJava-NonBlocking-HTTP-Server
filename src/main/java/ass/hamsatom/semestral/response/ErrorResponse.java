package ass.hamsatom.semestral.response;

import java.nio.channels.SocketChannel;
import java.util.Optional;

import javax.annotation.Nonnull;

import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.constat.HttpMethod;
import ass.hamsatom.semestral.request.ParsedRequest;
import lombok.Getter;

/**
 * Represents response to invalid request or response in case of error on Server
 *
 * @author Tomáš Hamsa on 11.05.2017.
 */
public class ErrorResponse implements Response {

  /**
   * Error code according to http representation
   */
  @Getter
  private final HttpCode code;

  /**
   * Http method that request this data
   */
  @Getter
  private final HttpMethod method;

  /**
   * Channel from which the request arrived
   */
  @Getter
  private final SocketChannel client;

  public ErrorResponse(HttpCode code, @Nonnull ParsedRequest originRequest) {
    method = originRequest.getMethod();
    client = originRequest.getClient();
    this.code = code;
  }

  @Nonnull
  @Override
  public Optional<byte[]> getData() {
    return Optional.empty();
  }

  @Override
  public boolean isCacheable() {
    return false;
  }

  @Override
  public boolean isValid() {
    return false;
  }
}
