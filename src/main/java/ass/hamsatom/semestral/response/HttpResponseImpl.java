package ass.hamsatom.semestral.response;

import java.nio.channels.SocketChannel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Tomáš Hamsa on 16.05.2017.
 */
@AllArgsConstructor
public class HttpResponseImpl implements HttpResponse {

  @Getter
  private final SocketChannel client;

  @Getter
  private final byte[] data;
}
