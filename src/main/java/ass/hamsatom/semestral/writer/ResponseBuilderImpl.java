package ass.hamsatom.semestral.writer;

import ass.hamsatom.semestral.constat.Constants;
import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.constat.HttpMethod;
import ass.hamsatom.semestral.response.HttpResponse;
import ass.hamsatom.semestral.response.HttpResponseImpl;
import ass.hamsatom.semestral.response.Response;
import com.google.inject.Inject;
import java.io.ByteArrayOutputStream;
import javax.annotation.Nonnull;
import lombok.Cleanup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Maps {@link Response} to HTTP responses and adds it to {@link WritingQueue}
 */
public class ResponseBuilderImpl implements ResponseBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(ResponseBuilderImpl.class);

  /**
   * Version of HTTP protocol used for all server responses
   */
  private static final String HTTP_VERSION = "HTTP/1.1";

  /**
   * Bytes in defined Charset of HTTP end line separator
   */
  private static final byte[] HTTP_EOL_BYTES = Constants.HTTP_LINE_SEPARATOR
      .getBytes(Constants.TELNET_CHARSET);

  /**
   * Bytes of header with authentication type
   */
  private static final byte[] AUTHENTICATE_HEADER = "WWW-Authenticate: Basic realm=\"User Visible Realm\""
      .getBytes(Constants.TELNET_CHARSET);

  /**
   * Queue that writes responses to client's {@link java.nio.channels.SocketChannel}
   */
  private final WritingQueue writingQueue;


  /**
   * Creates new ResponseBuilder that maps {@link Response} to HTTP responses and adds it to {@link
   * WritingQueue}
   *
   * @param writingQueue {@link WritingQueue} used for writing the responses
   */
  @Inject
  public ResponseBuilderImpl(@Nonnull WritingQueue writingQueue) {
    this.writingQueue = writingQueue;
  }

  /**
   * Uses data from {@link Response} to create {@link HttpResponse}. Then add the http response to
   * given {@link WritingQueue} to be written
   *
   * @param response Response that will be mapped to {@link HttpResponse} and added to {@link
   * WritingQueue}
   * @throws Exception If mapping the response failed
   */
  @Override
  public void accept(@Nonnull Response response) throws Exception {
    String head =
        HTTP_VERSION + " " + response.getCode().getValue() + " " + response.getCode()
            + Constants.HTTP_LINE_SEPARATOR;

    LOG.debug(head);

    @Cleanup ByteArrayOutputStream responseBody = new ByteArrayOutputStream();
    responseBody.write(head.getBytes(Constants.TELNET_CHARSET));

    // If request didn't provide authorization inform about authorization
    if (response.getCode() == HttpCode.UNAUTHORIZED) {
      responseBody.write(AUTHENTICATE_HEADER);
      responseBody.write(HTTP_EOL_BYTES);
    }

    // If the HTTP request is GET also write data to channel
    if (response.getMethod() == HttpMethod.GET && response.getData().isPresent()) {
      responseBody.write(HTTP_EOL_BYTES);
      // Optional.present already checked in if statement
      responseBody.write(response.getData().get());
    }

    responseBody.write(HTTP_EOL_BYTES);
    responseBody.flush();

    HttpResponse httpResponse = new HttpResponseImpl(response.getClient(),
        responseBody.toByteArray());
    writingQueue.add(httpResponse);
  }

  @Override
  public void endWriting() {
    writingQueue.stop();
  }
}