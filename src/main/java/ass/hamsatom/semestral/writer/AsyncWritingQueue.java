package ass.hamsatom.semestral.writer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ass.hamsatom.semestral.response.HttpResponse;
import io.reactivex.Observable;
import io.vavr.control.Try;

/**
 * Queue that writes http responses to {@link SocketChannel} asynchronously and in non-blocking way
 *
 * @author Tomáš Hamsa on 16.05.2017.
 */
public class AsyncWritingQueue implements WritingQueue {

  private static final Logger LOG = LoggerFactory.getLogger(AsyncWritingQueue.class);

  /**
   * Selector used for blocking in all clients's channels
   */
  private final Selector selector;

  /**
   * Queue with new new channels. Elements are inserted to this queue from another thread.
   */
  private final Queue<SocketChannel> queue;

  /**
   * Maps {@link SocketChannel} and data written into it
   */
  private final Map<SocketChannel, ByteBuffer> bufferMap;

  /**
   * Flag that is true if queue was stopped, false otherwise
   */
  private volatile boolean finished;

  /**
   * Starts the writing queue on another thread
   *
   * @throws IOException if opening the {@link Selector} failed
   */
  public AsyncWritingQueue() throws IOException {
    selector = Selector.open();
    queue = new ConcurrentLinkedQueue<>();
    bufferMap = new ConcurrentHashMap<>();
    new Thread(this::run).start();
  }

  /**
   * Writes data from all responses to their client's channel. Writing can be stopped using stop
   * method.
   */
  private void run() {
    LOG.debug("Started writing thread...");

    while (!finished) {
      registerChannels();
      Try.of(selector::select)
          .onFailure(e -> LOG.warn("Exception while blocking with selector", e));

      Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
      Observable.<SelectionKey>create(emitter -> {
        while (iterator.hasNext()) {
          SelectionKey next = iterator.next();
          emitter.onNext(next);
          iterator.remove();    // this is important, as selector only adds but does
          // not remove
        }
        emitter.onComplete();
      }).filter(SelectionKey::isValid).filter(SelectionKey::isWritable).cache()
          .map(key -> (SocketChannel) key.channel()).subscribe(channel -> Try.of(() -> {
        ByteBuffer bufferToWrite = bufferMap.get(channel);
        channel.write(bufferToWrite);
        if (!bufferToWrite.hasRemaining()) {
          channel.close();
        }
        return null;
      }));
      bufferMap.forEach((key, value) -> {
        if (!key.isConnected() || !key.isOpen()) {
          bufferMap.remove(key);
        }
      });
    }

    closeChannels();
    LOG.debug("Ended writing thread...");
  }

  /**
   * Closes all channels stored in collections
   */
  private void closeChannels() {
    Observable.fromIterable(queue).subscribe(SocketChannel::close);
    Observable.fromIterable(bufferMap.keySet()).subscribe(SocketChannel::close);
  }

  /**
   * Register channels from queue to local selector
   */
  private void registerChannels() {
    Observable.fromIterable(queue).forEach(socket -> {
      queue.poll();
      socket.register(selector, SelectionKey.OP_WRITE);
    });
  }

  @Override
  public void add(@Nonnull HttpResponse response) {
    ByteBuffer data = ByteBuffer.wrap(response.getData());
    SocketChannel socket = response.getClient();
    bufferMap.put(socket, data);
    queue.add(socket);
    // this wakes up blocking selector.select() so that loop can register sockets added to queue
    selector.wakeup();
  }

  @Override
  public void stop() {
    finished = true;
  }
}
