package ass.hamsatom.semestral.writer;

import ass.hamsatom.semestral.response.HttpResponse;
import javax.annotation.Nonnull;

/**
 * Class that writes responses to client's {@link java.nio.channels.SocketChannel} in a non-blocking
 * way
 *
 * @author Tomáš Hamsa on 18.05.2017.
 */
public interface WritingQueue {

  /**
   * Adds response to queue. The response will be processed and it's data written to the client's
   * {@link java.nio.channels.SocketChannel}
   *
   * @param response Response that will be written to {@link java.nio.channels.SocketChannel}
   */
  void add(@Nonnull HttpResponse response);

  /**
   * Stops the writing. That means that responses added after this call will not be written
   */
  void stop();

}
