package ass.hamsatom.semestral.filter;

import ass.hamsatom.semestral.request.ParsedRequest;
import ass.hamsatom.semestral.response.Response;
import io.reactivex.ObservableTransformer;

/**
 * Provides response according to validity of the request Maps all {@link ParsedRequest}s to {@link
 * Response} with appropriate code
 *
 * @author Tomáš Hamsa on 08.04.2017.
 */
public interface RequestFilter extends ObservableTransformer<ParsedRequest, Response> {

}
