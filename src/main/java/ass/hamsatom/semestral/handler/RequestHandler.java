package ass.hamsatom.semestral.handler;


import ass.hamsatom.semestral.request.ParsedRequest;
import ass.hamsatom.semestral.response.ErrorResponse;
import ass.hamsatom.semestral.response.Response;
import io.reactivex.ObservableTransformer;

/**
 * Provides handling of requests. That means that this class load data demanded by the {@link
 * ParsedRequest} and saves them into {@link Response}. If some error occurs than the response is
 * {@link ErrorResponse} with appropriate code.
 */
public interface RequestHandler extends ObservableTransformer<ParsedRequest, Response> {

}
