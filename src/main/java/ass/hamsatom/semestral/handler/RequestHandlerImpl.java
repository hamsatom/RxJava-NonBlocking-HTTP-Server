package ass.hamsatom.semestral.handler;

import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.request.ParsedRequest;
import ass.hamsatom.semestral.response.CacheResponse;
import ass.hamsatom.semestral.response.ErrorResponse;
import ass.hamsatom.semestral.response.FileResponse;
import ass.hamsatom.semestral.response.Response;
import ass.hamsatom.semestral.util.Cache;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.schedulers.Schedulers;
import javax.annotation.Nonnull;

/**
 * Process all give request in parallel. Tries to use {@link Cache} as much as possible. If the
 * {@link ParsedRequest} demands data that are in {@link Cache} than data to {@link Response} are
 * loaded from Cache otherwise they are loaded from file and than cached.
 *
 * @author Tomáš Hamsa on 02.04.2017.
 */
public class RequestHandlerImpl implements RequestHandler {

  /**
   * Creates {@link Observable} with response from {@link Cache}
   */
  @Nonnull
  private Observable<CacheResponse> getCacheObservable(@Nonnull ParsedRequest parsedRequest) {
    return Observable.just(new CacheResponse(parsedRequest)).filter(CacheResponse::isValid);
  }

  /**
   * Creates {@link Observable} with response from File. Also caches the response to {@link Cache}
   * if the file was read successfully
   */
  @Nonnull
  private Observable<FileResponse> getFileObservable(@Nonnull ParsedRequest parsedRequest) {
    return Observable.just(new FileResponse(parsedRequest)).filter(FileResponse::isValid)
        .doOnNext(fileResponse -> {
          if (fileResponse.isCacheable()) {
            fileResponse.getData().ifPresent(data -> Cache
                .cacheInMemory(parsedRequest.getKey().toString(), data));
          }
        });
  }

  /**
   * Handles all request and map them to response
   *
   * @param requestObs - observable that contains request objects to be transformed into response
   * @return transformed observable that contains response
   */
  @Override
  @Nonnull
  public ObservableSource<Response> apply(@Nonnull Observable<ParsedRequest> requestObs) {
    return requestObs.flatMap(request -> Observable.just(request)
        .subscribeOn(Schedulers.io())
        .to(obsRequest -> Observable.<Response>concat(getCacheObservable(request),
            getFileObservable(request))
            .first(new ErrorResponse(HttpCode.INTERNAL_SERVER_ERROR, request))
            .toObservable()));
  }
}

