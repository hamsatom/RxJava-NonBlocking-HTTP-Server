package ass.hamsatom.semestral.request;

import ass.hamsatom.semestral.constat.HttpMethod;
import java.nio.channels.SocketChannel;
import java.nio.file.Path;
import javax.annotation.Nonnull;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Tomáš Hamsa on 11.05.2017.
 */
@AllArgsConstructor
public class ParsedRequestImpl implements ParsedRequest {

  @Getter
  @Nonnull
  private final SocketChannel client;

  @Getter
  @Nonnull
  private final Path key;

  @Getter
  @Nonnull
  private final String base64dAuthentication;
  @Getter
  @Nonnull
  private final HttpMethod method;
}
