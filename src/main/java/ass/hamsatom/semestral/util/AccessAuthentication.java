package ass.hamsatom.semestral.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ass.hamsatom.semestral.constat.Constants;
import ass.hamsatom.semestral.request.ParsedRequest;

/**
 * Provides coding and encoding of protected data.
 *
 * @author Tomáš Hamsa on 08.04.2017.
 */
public final class AccessAuthentication {

  private static final Logger LOG = LoggerFactory.getLogger(AccessAuthentication.class);
  /**
   * Type of encoding
   */
  private static final String ENCRYPTION_FORMAT = "SHA-256";
  /**
   * String used before password so the result cannot be found in a rainbow table
   */
  private static final String SALT_PREFIX = "awYF4LnlHZHR4o2ZDdFC0wSS5zxk6hU7b2exOyWMeHhJ3D6Gq7vIjN59N1Cazx0EfEFuGpDBrpzp4cPUMFzScTiPe6zJiIowAAvVlQg7DLQI8I";
  /**
   * String used after password so the result cannot be found in a rainbow table
   */
  private static final String SALT_POSTFIX = "aPYMTRkez9mO5xFgcYS7Kh5giAiPl53diyN9YJOTNKdHTlLX6irRxXTUE1Qqtr0KisTSqIOEjUI3AICOVVjSMBsvL3krobXPxCh5e1BoMMP8xt";

  /**
   * Symbol that separates username and password
   */
  private static final String AUTHENTICATION_SEPARATOR = ":";

  /**
   * As this is utility class with static methods and field this class should not be ever
   * initialized
   *
   * @throws IllegalAccessException if someone tries to initialize this class
   */
  private AccessAuthentication() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  /**
   * Determines if the parsedRequest should have access to folder according to provided password and
   * username.
   *
   * @param parsedRequest ParsedRequestImpl hat wants to get some file
   * @return {@code true} if folder is not protected so any parsedRequest can enter or if
   * parsedRequest have valid password and username, {@code false} otherwise
   * @throws IOException if the file that contains information about user privileges cannot be read
   */
  public static boolean hasAccess(@Nonnull Path root,
      @Nonnull ParsedRequest parsedRequest) throws IOException {

    Optional<Path> highestAuthorizationFile = findHighestAuthenticator(root, parsedRequest);

    if (!highestAuthorizationFile.isPresent()) {
      return true;
    }

    String expectedEntry = decodeAuthentication(parsedRequest);
    return Files.lines(highestAuthorizationFile.get()).anyMatch(Predicate.isEqual(expectedEntry));
  }

  /**
   * Decodes user authentication in base 64 to String with format username<separator>encoded
   * password
   *
   * @param parsedRequest Request from which the authentication will be decoded
   * @return decoded string that should be present in authorization file
   */
  @Nonnull
  private static String decodeAuthentication(@Nonnull ParsedRequest parsedRequest) {
    return Stream.of(parsedRequest.getBase64dAuthentication())
        .map(data -> Base64.getDecoder().decode(data))
        .map(bytes -> new String(bytes, Constants.TELNET_CHARSET))
		.map(mixedData -> {
          int separatorPosition = mixedData.indexOf(AUTHENTICATION_SEPARATOR);
          String username = mixedData.substring(0, separatorPosition + 1);
          String password = mixedData.substring(separatorPosition + 1);
          return username + encodePassword(password);
        }).collect(Collectors.joining());
  }

  /**
   * Provides first htaccess file. First means that the htacees is the highest htaccess file in fle
   * tree.
   *
   * @param root The highest folder where htaccess can be present. The file search is done
   * recursively from this location
   * @param parsedRequest Request from which we will obtain location of data the request demands.
   * The htaccess file cannot be located under request's location
   * @return Path to highest htaccess file, {@link Optional}.empty() if no htaccess file was found
   * @throws IOException If reading files failed
   */
  @Nonnull
  private static Optional<Path> findHighestAuthenticator(@Nonnull Path root,
      @Nonnull ParsedRequest parsedRequest)
      throws IOException {
    Path parentFolder = parsedRequest.getKey().getParent().toAbsolutePath().normalize();
    return Files.walk(root).parallel()
        .filter(parentFolder::startsWith).flatMap(dir -> {
          try {
            return Files.list(dir);
          } catch (IOException e) {
            LOG.warn("Error while finding highest " + Constants.AUTHORIZATION_FILE, e);
          }
          return Stream.empty();
        })
        .filter(Files::isRegularFile)
        .filter(file -> file.endsWith(Constants.AUTHORIZATION_FILE))
        .findFirst();
  }

  /**
   * Encoded password according to desired standard
   *
   * @param cleanPassword char password to be coded
   * @return encrypted password
   */
  @Nonnull
  private static String encodePassword(@Nonnull String cleanPassword) {
    try {
      MessageDigest encoder = MessageDigest.getInstance(ENCRYPTION_FORMAT);
      encoder.update(SALT_PREFIX.getBytes(Constants.TELNET_CHARSET));
      encoder.update(encoder.digest(cleanPassword.getBytes(Constants.TELNET_CHARSET)));
      encoder.update(SALT_POSTFIX.getBytes(Constants.TELNET_CHARSET));
      return Base64.getEncoder().encodeToString(encoder.digest());
    } catch (NoSuchAlgorithmException e) {
      LOG.error("Invalid hashing algorithm name", e);
      return "";
    }
  }
}
