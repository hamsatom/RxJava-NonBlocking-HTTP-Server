package ass.hamsatom.semestral.parser;

import ass.hamsatom.semestral.request.HttpRequest;
import ass.hamsatom.semestral.request.ParsedRequest;
import ass.hamsatom.semestral.response.Response;
import ass.hamsatom.semestral.server.Server;
import io.reactivex.ObservableTransformer;

/**
 * Parses raw {@link HttpRequest}s received in {@link Server} to {@link ParsedRequest} and then maps
 * them to appropriate {@link Response}
 *
 * @author Tomáš Hamsa on 11.05.2017.
 */
public interface HttpParser extends ObservableTransformer<HttpRequest, Response> {

}
